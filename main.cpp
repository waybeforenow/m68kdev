#include "Musashi/m68k.h"

#include <cstdio>
#include <map>

std::map<unsigned int, unsigned int> memory;

unsigned int m68k_read_memory_8(unsigned int address) {
  return m68k_read_memory_32(address) & 0x000000ff;
}
unsigned int m68k_read_memory_16(unsigned int address) {
  return m68k_read_memory_32(address) & 0x0000ffff;
}
unsigned int m68k_read_memory_32(unsigned int address) {
  printf("lookup at addr %x\n", address);

  const auto lookup = memory.find(address);
  if (lookup == memory.end()) {
    return 0;
  }
  return lookup->second;
}

void m68k_write_memory_8(unsigned int address, unsigned int value) {
  m68k_write_memory_32(address, value & 0x000000ff);
}
void m68k_write_memory_16(unsigned int address, unsigned int value) {
  m68k_write_memory_32(address, value & 0x0000ffff);
}
void m68k_write_memory_32(unsigned int address, unsigned int value) {
  memory.insert_or_assign(address, value);
}

int main() {
  m68k_init();
  m68k_set_cpu_type(M68K_CPU_TYPE_68000);
  m68k_pulse_reset();

  const auto cycles = m68k_execute(1);
  printf("%x\n", cycles);

  return 0;
}
